package plugins;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import market.Market;
import operation.Order;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class BinanceImplementation implements Market {
	private final String name = "BINANCE";
	private String binanceUrl = "https://api.binance.com/api/v3";
	private String ticketPrice = "/ticker/price?symbol=";
	private String resultingPair = "USDT";
	private int timeout = 60000;

	@Override
	public double getPrice(String symbol) {
		HttpURLConnection con;
		String result = "";
		try {
			con = createConnection(symbol);
			con.setConnectTimeout(timeout);
			con.setReadTimeout(timeout);
			result = inputStreamToString(con.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return !result.isEmpty() ? extractPrice(result) : -1;
	}

	private HttpURLConnection createConnection(String symbol) throws IOException {
		URL url = new URL(binanceUrl + ticketPrice + symbol + resultingPair);
		return (HttpURLConnection) url.openConnection();
	}

	private String inputStreamToString(InputStream response) throws IOException {
		if(response == null ) return "";
		BufferedReader in = new BufferedReader(new InputStreamReader(response));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		return content.toString();
	}

	private double extractPrice(String response){
		JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
		return (jsonObject.get("price") != null)? jsonObject.get("price").getAsDouble(): -1;
	}

	@Override
	public String getName() {
		return name;
	}
	@Override
	public String placeOrder(Order order) {
		return generateCode();
	}

	private String generateCode() {
		String orderCode = "";
		for(int i = 0; i < 3; i++){
			orderCode += generateRandomLetter() + generateRandomNumber();
		}
		return orderCode;
	}


	private String generateRandomNumber() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26));

	}

	private String generateRandomLetter() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26)+ 'a');
	}
	
	public void setTimeout(int timeout)
	{
		this.timeout = timeout;
	}
}
