package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import asset.Asset;
import operation.OperationType;
import operation.Order;
import plugins.CryptoMarketImplementation;

public class CryptoMarketImplementationTest {
	private CryptoMarketImplementation cryptoMarket;
	double quantity;
	double marketPrice;
	Asset asset;
	OperationType operationType;
	
    
	@Before
	public void init() {
		cryptoMarket = new CryptoMarketImplementation();
		quantity = 5;
		marketPrice = 100;
		asset = new Asset("BITCOIN","BTC");
	}
    
	@Test
	public void testPlaceOrder() {
		Order order = new Order(100, new Date(), 100, asset, OperationType.BUY);
		assertTrue(cryptoMarket.placeOrder(order)==null);
	}
	
	@Test
    public void testGetPrice()  
    {
		assertTrue(cryptoMarket.getPrice("BTC")>0); 
    }
    
    @Test
    public void testGetName()
    {
    	assertTrue(cryptoMarket.getName() == "CRYPTOMARKET");
    }
}
