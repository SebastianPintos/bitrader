package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;

import asset.Asset;
import operation.OperationType;
import operation.Order;
import plugins.BinanceImplementation;


public class BinanceImplementationTest {
	private BinanceImplementation binance;
	double quantity;
	double marketPrice;
	Asset asset;
	OperationType operationType;
    
	@Before
	public void init() {
		binance = new BinanceImplementation();
		quantity = 5;
		marketPrice = 100;
		asset = new Asset("BITCOIN","BTC");
	}
    
	@Test
	public void testPlaceOrder() {	
		assertTrue(binance.placeOrder(new Order(marketPrice, new Date(), marketPrice, asset, operationType)).length()>=0);
	}
    
    @Test
    public void testGetName()
    {
    	assertTrue(binance.getName().contentEquals("BINANCE"));
    }
	
}
